## Weather_Station_project

## Description
The weather station with STM32 can include several sensors to measure different meteorological variables. These sensors can be connected to the microcontroller through different interfaces, such as I2C, SPI, UART, among others. The STM32 can also include wireless interfaces such as Bluetooth or Wi-Fi to send the collected data to a remote database or web server.

## Support
sagiraldogo@gmail.com

## Authors 
Santiago Giraldo Gomez
